# Ensemble Learning for Door Classification

This model is intended to classify whether the container includes a door or not

## Train on custom dataset

The author has provided 3 pretrained weights in ```weights``` folder. However, if you would like to continue training or train on a different dataset, you could execute the command below, with train images placed in ```dataset/train``` folder

```
python main.py --mode train
```

## Test on your dataset

Analogously, put your test images in ```test``` folder and execute the command below.

```
python main.py --mode test
```

## Citation

```
@misc{ensemble_learning_2020
	title={Scott Nguyen},
	publisher={gitlab}
}
```


