from keras.layers import Conv2D, Activation, MaxPooling2D, Flatten, Dense, Dropout
from keras.models import Sequential
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
import os
import cv2
import numpy as np


def type_1_graph(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=(input_shape, input_shape, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    return model


def type_2_graph(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=(input_shape, input_shape, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))


def type_3_graph(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=(input_shape, input_shape, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    return model


class Dataset:

    def __init__(self, train_dir, test_dir=None):

        self.train_dir = train_dir
        self.test_dir = test_dir

    def prepare_batches(self, test_size=0.2, shuffle=True):

        files = os.listdir(self.train_dir)
        data = []
        labels = []
        for file in files:
            img = cv2.imread(os.path.join(self.train_dir, file))
            img = img.reshape(300, 300, 3)
            data.append(img)
            if 'no_door' in file:
                labels.append(0)
            else:
                labels.append(1)
        
        data = np.asarray(data)
        labels = np.asarray(labels)

        train_data, val_data, train_label, val_label = train_test_split(data, labels, test_size=test_size, shuffle=shuffle)
        self.train_data = train_data
        self.val_data = val_data
        self.train_label = train_label
        self.val_label = val_label


class ScottModel:

    def __init__(self, type, input_shape=300):
        self.input_shape = input_shape
        self.type = type

    def build(self):
        model_graph = [type_1_graph, type_2_graph, type_3_graph]
        self.model = model_graph[self.type](self.input_shape)

    def train(self, dataset, batch_size, model_dir, epochs=30):

        self.model.compile(loss='binary_crossentropy',\
                           optimizer='adam', metrics=['accuracy'])
        file_path = os.path.join(model_dir, "containers-{val_accuracy:.2f}-{epoch:02d}.h5")
        checkpoint = ModelCheckpoint(file_path, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')

        callbacks_list = [checkpoint]
        self.model.fit(dataset.train_data, dataset.train_label,\
                  batch_size=batch_size,\
                  epochs=epochs,\
                  callbacks=callbacks_list,\
                  validation_data=(dataset.val_data, dataset.val_label))
