"""
Ensemble Learning 
Trained on Container Dataset to classify whether it includes a door or not

Contributor: Scott Nguyen <thongnguyen050999@gmail.com>
"""

import os
import cv2
import argparse
import keras
import tensorflow as tf
from keras.models import load_model
from models import Dataset, ScottModel

config = tf.compat.v1.ConfigProto( device_count = {'GPU': 1 } ) 
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.5
sess = tf.compat.v1.Session(config=config) 
tf.compat.v1.keras.backend.set_session(sess)

parser = argparse.ArgumentParser(description='Door Classification')
parser.add_argument('--n_epoch', type=int, default=30)
parser.add_argument('--type', type=int, default=0)
parser.add_argument('--num_models', type=int, default=3)
parser.add_argument('--batch_size', type=int, default=16)
parser.add_argument('--mode', type=str)
parser.add_argument('--test_img_dir', type=str, default='./dataset/test')
parser.add_argument('--train_img_dir', type=str, default='./dataset/train')
parser.add_argument('--save_dir', type=str, default='./result')
parser.add_argument('--model_dir', type=str, default='./weights')
args = parser.parse_args()


def prepareFont():

	font = cv2.FONT_HERSHEY_DUPLEX
	org = (50, 50)
	fontScale = 1
	color = (0, 0, 255)
	thickness = 2

	return font, org, fontScale, color, thickness


def decorationPrint():
	for i in range(50):
		print('=', end='')
	print()


def main():

	if args.mode == 'train':
		dataset = Dataset(args.train_img_dir)
		dataset.prepare_batches()
		scott_model = ScottModel(args.type)
		scott_model.build()
		scott_model.train(dataset, args.batch_size, args.model_dir)

	else:
		model_weights = os.listdir(args.model_dir)
		model_weights = sorted(model_weights,reverse=True)

		models = []
		num = 0
		for weight in model_weights:
			models.append(load_model(os.path.join(args.model_dir, weight)))
			num += 1
			if num == args.num_models:
				break

		listFiles = list(filter(
			lambda img: img[-3:].lower() == 'jpg' or img[-3:].lower() == 'png', os.listdir(args.test_img_dir)))
		imgDir = args.test_img_dir
		saveDir = args.save_dir

		for file in listFiles:

			img = cv2.imread(os.path.join(imgDir,file))
			img = cv2.resize(img, (300, 300), interpolation=cv2.INTER_AREA)
			image = img.copy()
			img = img.reshape(1, 300, 300, 3)

			i = 1

			num_vote = num_reject = 0
			for model in models:
				result = float(model.predict(img)[0][0])
				if result >= 0.5:
					num_vote += 1
				else:
					num_reject += 1

			ans = int(num_vote >= num_reject)

			font, org, fontScale, color, thickness = prepareFont()

			if (ans):
				print(file+': DOOR')
				decorationPrint()
				image = cv2.putText(image, 'DOOR', org, font,
									fontScale, color, thickness, cv2.LINE_AA)
			else:
				print(file+': NO DOOR')
				decorationPrint()
				image = cv2.putText(image, 'NO DOOR', org, font,
									fontScale, color, thickness, cv2.LINE_AA)

			cv2.imwrite(os.path.join(saveDir,file), image)


if __name__ == '__main__':
	main()
